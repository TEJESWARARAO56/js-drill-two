let inventory = require('./data.cjs');

function problemOne(inventory, searchid) {
    // console.log(typeof inventory, inventory)
    if (inventory === undefined || searchid === undefined || inventory instanceof String) {
        return []
    } else if (
        typeof inventory === 'object' &&
        !Array.isArray(inventory) &&
        inventory !== null
    ) {
        return []
    }
    let requiredData = []
    requiredData = inventory.filter((carobject) => {
        return carobject.id === searchid
    });
    return requiredData
}

if (require.main === module) {
    const result = problemOne(inventory, 33);
    // const result = problemOne(new String("hello"), 33);
    console.log(result)
}
module.exports = problemOne
/* ==== Problem #1 ====
The dealer can't recall the information for a car with an id of 33 on his lot. 
Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. 
Then log the car's year, make, and model in the console log in the format of:

"Car 33 is a *car year goes here* *car make goes here* *car model goes here*" */




