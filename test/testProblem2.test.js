let inventory=require('../data.cjs');
let problemTwo=require('../problem2.cjs')

test('checking data of last car', () => {
  expect(problemTwo(inventory)).toStrictEqual({ id: 50, car_make: 'Lincoln', car_model: 'Town Car', car_year: 1999 });
});