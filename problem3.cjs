let inventory = require('./data.cjs');

function problemThree(inventory) {
    if (inventory === []) {
        return []
    }
    let carModelNames = inventory.map((carobject) => {
        return carobject.car_model;
    })
    let sortedBrands = carModelNames.sort((a, b) => {
        return a.toUpperCase().localeCompare(b.toUpperCase());
    });
    console.log(sortedBrands);
    return sortedBrands;
}

if (require.main === module) {
    problemThree(inventory);
}

module.exports = problemThree;

/* ==== Problem #3 ====
The marketing team wants the car models listed alphabetically on the website.
 Execute a function to Sort all the car model names into alphabetical order and 
 log the results in the console as it was returned. */
